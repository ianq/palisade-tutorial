# A Machine Learning based introduction to PALISADE and CKKS

## Introduction to PALISADE and a simple application

1) Training on a single vector of the Iris dataset. This is phrased as a linear regression because it is simpler
2) The parameters that we need to specify and what they mean for our application
3) Ciphertext Refreshing aka "Interactive" approach.

Note: check the link at the very bottom for the complete source code. Sections have been omitted in this page to reduce clutter.

---

We first need to import all the libraries we need
```C++
#include "palisade.h"
#include <iomanip>
using namespace lbcrypto;
```
Instructions to install PALISADE can be found here: [PALISADE-Dev build instructions](https://gitlab.com/palisade/palisade-development#build-instructions). If you're new to PALISADE and C++ I highly recommend bookmarking the [PALISADE Doxygen page](https://palisade.gitlab.io/palisade-development/files.html) which contains the documentation for the library.

## Training on the Iris dataset:

In this section we show 

- how to create a cryptocontext and 
- code for training on a single vector of the iris dataset.

One important thing to note is that the results you obtain may be slightly different from a plaintext version or a numpy version because CKKS accumulates noise as we execute more and more operations. 

We briefly introduce the parameters used below but defer further discussion to later.

```C++
  CryptoContext<DCRTPoly> cc = CryptoContextFactory<DCRTPoly>::genCryptoContextCKKS(
      multDepth, scalingFactorBits, batchSize
  );

  std::cout << "Ring Dimension: " << cc->GetRingDimension() << '\n';

  cc->Enable(ENCRYPTION);
  cc->Enable(SHE);
  cc->Enable(LEVELEDSHE);
  auto keys = cc->KeyGen();
  cc->EvalMultKeyGen(keys.secretKey);
  cc->EvalSumKeyGen(keys.secretKey);
```
We create a crypto context which takes our chosen parameters: 
`multDepth` - number of sequential multiplications we can do, 
`scalingFactorBits` - the scaling factor 
`batchSize` - how many messages we pack into a cyphertext. Homomorphic encryption is slow but can be sped up by conducting operations over batches of data (via SIMD) 

### Training setup

You may have some questions while reading through this section such as:

- why do we operate on complex numbers?
- Why do we have to convert from our raw data to a packedplaintext before encrypting? 

```C++
  /////////////////////////////////////////////////////////////////
  //Setup the weights, biases and encrypt all of them
  /////////////////////////////////////////////////////////////////

  std::vector<std::complex<double>> weights = {1, 1, -0.1, -1.5, 10.01};
  std::vector<std::complex<double>> singleLabelAsVector = {(float) labels[0]};
  std::vector<std::complex<double>> biasFeatures = features[0];
  auto it = biasFeatures.begin();
  std::cout << "Vec 0 before adding bias: " << biasFeatures << '\n';
  biasFeatures.insert(it, std::complex<double>(1, 0));
  std::cout << "Vec 0 after adding bias: " << biasFeatures << '\n';

  auto singleFeature = cc->MakeCKKSPackedPlaintext(biasFeatures);
  auto singleLabel = cc->MakeCKKSPackedPlaintext(singleLabelAsVector);
  auto packedWeights = cc->MakeCKKSPackedPlaintext(weights);
  auto encFeature = cc->Encrypt(keys.publicKey, singleFeature);
  auto encLabel = cc->Encrypt(keys.publicKey, singleLabel);
  auto encWeights = cc->Encrypt(keys.publicKey, packedWeights);

  double alpha = 0.01;
  Plaintext plaintextMultiUse;
  if (verbose) {
    std::cout << "Label: " << singleLabel << '\n';
  }
  std::vector<double> costs;
```

### Training Loop

The following section should be familiar to you if you've taken an introductory ML class. Having said that, pay attention to the part immediately after

```C++
  /////////////////////////////////////////////////////////////////
  //Start training
  /////////////////////////////////////////////////////////////////
  for (int i = 1; i < epochs + 1; i++) {  // offset the index by 1 for some modulus checking below
    if (verbose) {
      std::cout << "Epoch : " << i << '\n';
    }
    auto innerProd = cc->EvalSum(cc->EvalMult(encFeature, encWeights), batchSize);

    auto err = cc->EvalSub(innerProd, encLabel);
    cc->Decrypt(keys.secretKey, err, &plaintextMultiUse);
    plaintextMultiUse->SetLength(1);

    if (verbose) {
      std::cout << "Error: " << plaintextMultiUse->GetCKKSPackedValue()[0].real() << '\n';
    }

    auto costVec = plaintextMultiUse->GetCKKSPackedValue();
    auto cost = costVec[0].real() * costVec[0] + costVec[1].real() * costVec[1].real() +
        costVec[2].real() * costVec[2] + costVec[3].real() * costVec[3].real();

    if (verbose) {
      std::cout << "cost: " << cost * 0.5 << '\n';
    }
    costs.emplace_back(1.0 * cost.real());

    // derivative:
    // J(w) = (y-Wx)
    // /partial J(w) = x.T * (y-Wx)  ( Equivalent to dot prod)

    auto err_partial_w = cc->EvalSum(cc->EvalMult(encFeature, err), batchSize);
    auto update = cc->EvalMult(alpha, err_partial_w);  // Scale by learning rate

    cc->Decrypt(keys.secretKey, update, &plaintextMultiUse);
    plaintextMultiUse->SetLength(4);
    if (verbose) {

      std::cout << "Update: " << plaintextMultiUse << '\n';
    }
    encWeights = cc->EvalSub(encWeights, update);
```

The following section is within the epoch loop (like most of the code above). You should notice a few things:

- it seems like we're refreshing something every few epochs
- we're decrypting our weights with our cryptocontext then immediately encrypting the weights again. What gives? 
```C++
    if ((refreshEvery != 0) && (i % refreshEvery == 0)) {
      if (verbose) {
        std::cout << "Refreshing the weights for epoch: " << i << " \n";
      }
      Plaintext clearWeights;
      cc->Decrypt(keys.secretKey, encWeights, &clearWeights);  // We don't actually use this. We use it to decrpt
      auto oldWeights = clearWeights->GetCKKSPackedValue();
      clearWeights->SetLength(4);
      encWeights = cc->Encrypt(keys.publicKey, cc->MakeCKKSPackedPlaintext(oldWeights));
  }
```

The reason for our decrypting and re-encrypting has to do with the `multDepth` parameter that we specified earlier. As mentioned much earlier, as we do operations on our ciphertexts, we accumulate noise. If this noise gets too large, our decryption will begin to fail. By decrypting and encrypting our results again, we are able to refresh this noise. 

However, there are a few caveats here:

1) only the party that has the secret key can do the encrypting. In the case of a dataEnclave-client the client would need to send the weights back to the dataEnclave. 

2) this is considered less secure compared to a FHE setup.  We discuss this further in [Tutorial 1.5: CKKS for the Curious](https://gitlab.com/ianq/palisade-tutorial/-/blob/master/tutorials/tutorial1.5.md)

## Parameter Discussion

### multDepth
```C++
// - Depth of multiplication supported:
// 	(a * b) + (c * d) has a mult depth of 1
// 	a * b * c has a mult depth of 2
// - We may come up on the term "towers" later and multDepthSmall is one less than that
// 	i.e multDepthSmall == (numTowers - 1)
// - Note: there's not really a 1-size-fits-all setup here so you'll need to know
// 	your underlying algorithm to use this effectively
// - It is possible to set an extremely high multDepth but this means that your computations will run slower and require much more space.

uint8_t multDepthSmall = 5;
```

### scalingFactorBits

```C++
// - In the original paper they discuss a scaling factor which they multiply by
// 	to prevent rounding errors from destroying the significant figures during encoding
// - Note: this specifies the bit LENGTH of the scaling factor but not the scaling factor
//	itself
// - Note: I personally stick to 50
uint8_t scalingFactorBits = 40;
```

### batchSize

```C++
// - The number of slots to use. This is typically set to a power of 2 larger than
// the dimensionality of the data. Our iris dataset has 4 features and the next
// power of 2 is 8
int batchSize = 4096;
```

```C++
int main() {

  std::cout.precision(3);
  FeatureMatrixComplex features = {{5.7, 3.8, 1.7, 0.3}};
  LabelVector label_vector = {0};

  /*
   * Step 1: Parameter Discussion
   */

  uint8_t multDepthSmall = 5;
  uint8_t scalingFactorBits = 40;
  int batchSize = 4096;
  int refreshEvery = 1;
  create_context_and_train(multDepth, scalingFactorBits, batchSize, refreshEvery, features, label_vector);
}
```
To see the full code visit my [palisade-tutorial ](https://gitlab.com/ianq/palisade-tutorial/-/blob/master/src/tutorial1.cpp)on Gitlab.
