# A Machine Learning based introduction to PALISADE and CKKS

## Tutorial 1.5: CKKS for the curious

This tutorial is meant for ML practitioners who are interested in CKKS and Homomorphic Encryption in general. We attempt to make this section digestible but will also provide links for those who want a more in-depth reading.

We use the following acronyms:
Partially homomorphic encryption (PHE)
Somewhat homomorphic encryption (SHE)
Levelled Somewhat homomorphic encryption (LeveledSHE)
Fully homomorphic encryption (FHE)
Cheon-Kim-Kim-Song (CKKS) which was the new name after HEAAN.

## Homomorphic Encryption

There are several different levels of homomorphic encryption, a few of which we discuss briefly, but we focus on two primary levels: 
the SHE and the LeveledSHE, because the PALISADE library supports them.


The PHE encryption level allows either addition or multiplication operations on elements within its group. One notable property is that these algorithms are unbounded in that we can carry out any number of either additions or multiplications. Many popular encryption schemes fall into this category: for example, [RSA](https://people.csail.mit.edu/rivest/Rsapaper.pdf) and [ElGamal](https://ieeexplore.ieee.org/document/1057074), both of which support multiplications on their ciphertexts. Another PHE scheme is the [Paillier encryption scheme](https://www.researchgate.net/profile/Pascal_Paillier2/publication/249581677_Paillier_Encryption_and_Signature_Schemes/links/56b35be308ae156bc5fb1f1c/Paillier-Encryption-and-Signature-Schemes.pdf) which supports addition on its ciphertexts.

Somewhat homomorphic encryption supports both addition and multiplication logic gates instead of one-or-the-other as in the case of PHE. However, it still only supports a subset of circuits as it is now bounded in the number of operations it can do. As we carry out these operations, there is a non-zero amount of noise (accumulated errors) added to the result of the mathematical operation. Eventually, it is no longer possible to decrypt the ciphertext and obtain the expected result. We refer the interested reader to [Acar et al. A Survey on Homomorphic Encryption Schemes: Theory and Implementation](https://dl.acm.org/doi/pdf/10.1145/3214303)

Fully homomorphic encryption was not possible until the work of Craig Gentry in his [thesis](). In the thesis Gentry showed that it was possible to evaluate circuits of arbitrary depth via a process he termed bootstrapping. The fundamental idea of bootstrapping is that it is possible to "refresh" the ciphertext (thus reducing the noise). Gentry showed that by taking the decryption algorithm and decrypting both the ciphertext and the encrypted secret key ( encrypted using the public key ), it is possible to obtain a new ciphertext which has the same underlying plaintext but has the noise removed.

This process is equivalent to the following equation: \[\mathcal{C}_{i+1} = \text{Decrypt}(\mathcal{C}_i, \text{Enc}(Sk))\].

The equation describes the ciphertext at time i+1 and how it is derived from the original ciphertext. Nevertheless, the process of bootstrapping was (and still is) an expensive operation which can significantly slow down any application that requires a large number of additions or multiplications (such as in machine learning). 


## What is CKKS? 

If we were to get pedantic, CKKS isn't a homomorphic encryption scheme but is instead an approximate homomorphic encryption scheme.

CKKS is an encryption scheme that allows for operations on real numbers as opposed to just integers as is the case for BGV, and BFV which are other H.E. schemes that we do not discuss in this ML practitioners guide. 

Machine learning with homomorphic encryption was not fully supported until the CKKS scheme. Although various homomorphic encryption schemes emerged, and it was possible to do some forms of machine learning, all of them dealt with integers. This inability to operate on real numbers makes these schemes ill-suited for machine learning where we require some degree of stochasticity. However, work by [Cheon et al.](https://eprint.iacr.org/2016/421.pdf) in their paper entitled "Homomorphic Encryption for Arithmetic of Approximate Numbers" (also known as the CKKS scheme after the authors) general forms of machine learning became viable.  CKKS operates on complex numbers, but via the use of an embedding, it is possible to operate on the real numbers. 

### CKKS Intuition

To discuss the intuition behind CKKS, we first need to discuss how numbers are stored in computers. For every floating number, there is a significand and a scaling factor (comprised of a base and exponent)

For example, in a (truncated) representation of $\sqrt{2}$, we have: 

\[\sqrt{2} \approx 1414 * 10^{-3}\]

. The significand is 1414, and the scaling factor is $10^{-3}$ where 10 is the base and -3 is the exponent.

The critical insight is the following: if we are doing approximate arithmetic, for example, multiplying $\sqrt{2} * \sqrt{2}$ we have:

\begin{align*}
    1414 * 10^{-3} * 1414 * 10^{-3} &= 1999396 * 10^{-6}\\
    &\approx 2000 * 10^{-3}
\end{align*}

. This rounding off causes the stochasticity mentioned above. 

### The Guts

Earlier, we had mentioned that CKKS operates on the complex numbers and that these values can be converted to the real numbers via an encoding. We refer to the following image:

![CKKS Mapping](ckks_mapping.png)

We focus on the left half of the image involving the encoding. Note, however, that we gloss over specific details that we deem outside of the interest of most ML practitioners (even the ones reading this article). Notice, the encoding of the plaintext message consists of the tau function. The tau function is a transformation from the complex numbers to the rational numbers. We then scale this complex representation of the data by multiplying it with some delta to control for the precision loss in the rounding operation, which follows. 

We encourage users to watch the [video at this link](https://simons.berkeley.edu/talks/heaan-fhe). 

\subsubsection{Multiplication Depth}

In the levelledSHE case, we must specify a multiplicative depth which is the threshold for our noise. If our noise grows beyond the acceptable threshold, our decrypted results will no longer show the homomorphic property. A user may be tempted to set the number to be an overly large number, but this slows down the process of creating the cryptocontext and increase the memory requirements a great deal (the specifics of which we gloss over).

\subsubsection{Scaling Factor Bits}

In the original paper of \cite{cheon_homomorphic_2017}, they discuss a scaling factor, $\Delta$ which they multiply by to prevent rounding errors from growing too large (beyond which point our results no longer make sense). It is important to note that while the parameter in the CKKS paper is the scaling factor, the parameter here describes the number of bits. Careful selection of this parameter is essential, although the PALISADE library is reliable in notifying the user about errors.

\subsubsection{Batch Size}

The term "Batch size" in the context of PALISADE (and homomorphic encryption libraries in general) is not the same as a batch size in machine learning. A batch size in PALISADE describes the number of messages that are encoded in a single slot of our ciphertext. The message-packing allows the use of SIMD, which enables parallelism as we are no longer bound to calculating data sequentially.

Be sure to check out the accompanying code for this section: [Tutorial1.5 Cpp code](https://gitlab.com/ianq/palisade-tutorial/-/blob/master/src/tutorial1.5.cpp)

There are a few takeaways from the accompanying code:

1) if we carry out too many multiplications, we exceed our noise threshold, and our results become meaningless

2) We can avoid the noise issue by "refreshing" the ciphertext, a process called the interactive scheme. However, this comes with certain drawbacks (and is considered to be "less secure" as mentioned in the previous tutorial. The reason that it is "less secure" is because we need to be able to transmit the ciphertext between our private-key holder (a server) and our user (a client). Imagine a scenario where you have a central party which has access to a data lake, and multiple other parties querying data from the central party.

3) It is implied in the code, but we explicitly state that PALISADE does not support FHE, and you will need to refresh the ciphertext. Ideally, instead of recrypting, we would be able to use bootstrapping to keep the noise in check

## Further reading:

[Craig Gentry simplified paper](https://crypto.stanford.edu/craig/easy-fhe.pdf)

- We highly recommend this paper because it is a simplified version of some of the papers you will need to read to gain a deep appreciation of what is happening. 

[HEAAN Demystified](https://arxiv.org/abs/2003.04510)

- We recommend reading this before the actual HEAAN paper, specifically Section 3, which summarizes the HEAAN paper for non-cryptographers. 

[The actual CKKS/HEAAN paper](https://eprint.iacr.org/2016/421.pdf)

- This paper is a doozy and probably out of scope for someone who cares solely about the ML application of the work

