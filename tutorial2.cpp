/*
 * TODO: link to a PDF or something? Easier to show the equations. IDK
 *
 * We cover:
 * 0) What is CKKS and what makes it work? (Complex canonical)
    - raw data -> plaintext -> ciphertext
    - Relinearization
 * 1) Scaling factor, batch size, mult depth
    - Scaling factor bits
    - batch size + SIMD

 * 2) Showcase how to get around mult-depth issues
    - multiplication depth exceed -> too much noise now

 * 3) Misc:
    - rotation key

 * 4) FHE
 */
